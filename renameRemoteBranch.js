#!/usr/bin/env node

var path = require("path");
//var NodeGit = require("nodegit");
var argv = require("yargs")
  .strict()
  .version(function () {
    return require("../package").version;
  })
  .usage("Usage: $0 [-v|-vv] oldName newName")
  //.command(
  //  "renameRemoteBranch",
  //  "Rename a remote branch. Checkout the remote branch, rename it locally, push it,"
  //  + " delete the old remote branch, delete the local branch.")
  .example(
    "$0 -v branchName feature/branchName", "Rename the remote branch with name \"branchName\" to"
    + " \"feature/branchName\", and show what the command is doing.")
  .demand(
    2,
    2,
    "Two arguments required: the first argument is the old remote branch name, "
    + "the second the new remote branch name.")
  .option(
    "verbose",
    {
      alias: "v",
      count: true,
      describe: "v show more detail about what the command is doing, vv shows even more"
    })
  .help("h")
  .alias("h", "help")
  .epilog("Copyright 2015")
  .argv;

function warn() {
  argv.verbose >= 0 && console.log.apply(console, arguments);
}
function info() {
  argv.verbose >= 1 && console.log.apply(console, arguments);
}
function debug() {
  argv.verbose >= 2 && console.log.apply(console, arguments);
}


var cwd = path.resolve(process.cwd());

info("git repository: " + cwd);
console.log("old name: " + argv._[0]);
console.log("new name: " + argv._[1]);

console.log("Hello, world");

warn("Showing only important stuff");
info("Showing semi-important stuff too");
debug("Extra chatty mode");
